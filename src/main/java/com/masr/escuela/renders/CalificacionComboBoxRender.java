package com.masr.escuela.renders;

import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ComboitemRenderer;

import com.masr.escuela.model.Alumnos;
import com.masr.escuela.model.Materia;

public class CalificacionComboBoxRender implements ComboitemRenderer<Object> {

	@Override
	public void render(Comboitem comboitem, Object obj, int i) throws Exception {
		String nameClassObject = obj.getClass().getTypeName();
		if(nameClassObject.equalsIgnoreCase("com.masr.escuela.model.Alumnos")) {
			Alumnos objAlumno = (Alumnos) obj;
			comboitem.setLabel(objAlumno.getNombre() + " " + objAlumno.getApPaterno() + " " + objAlumno.getApMaterno());
			comboitem.setDescription("id:" + objAlumno.getIdUsuario());
			comboitem.setAttribute("dataAlumno", objAlumno);
		}else if(nameClassObject.equalsIgnoreCase("com.masr.escuela.model.Materia")) {
			Materia objMateria = (Materia) obj;
			comboitem.setLabel(objMateria.getNombre());
			comboitem.setAttribute("dataMateria", objMateria);
		}
	}
}
