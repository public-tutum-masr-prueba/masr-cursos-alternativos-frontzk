package com.masr.escuela.renders;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.zul.Button;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.masr.escuela.model.Calificacion;

public class CalificacionListBoxRender implements ListitemRenderer<Object> {

	@Override
	public void render(Listitem listItem, Object obj, int i) throws Exception {
		Calificacion dtoCalificacion = (Calificacion) obj;
		Listcell materia = new Listcell(dtoCalificacion.getMateria().getNombre());
		Listcell calificacion = new Listcell(String.valueOf(dtoCalificacion.getCalificacion()));
		Listcell fechaRegistro = new Listcell(String.valueOf(darFormatoFecha(dtoCalificacion.getFechaRegistro())));
		
		//fechaRegistro.appendChild(createBtnEliminar());
		
		listItem.appendChild(materia);
		listItem.appendChild(calificacion);
		listItem.appendChild(fechaRegistro);
		
		listItem.setAttribute("selected", dtoCalificacion);
	}
	
	private String darFormatoFecha(Date fecha) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.format(fecha);
	}
	
	private Button createBtnEliminar() {
		Button btnDelete = new Button("Eliminar");
		btnDelete.setStyle("background-color: #e92b2b; float:right");
		btnDelete.setClass("cssClase");
		return btnDelete;
	}
}
