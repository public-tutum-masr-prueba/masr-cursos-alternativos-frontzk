package com.masr.escuela.model;

import java.io.Serializable;
import java.util.List;

public class CalificacionWrapper implements Serializable {
    
	private static final long serialVersionUID = 7980399347014565589L;
	private List<Calificacion> listaCalificacion;
    private double promedio;
    
	public List<Calificacion> getListaCalificacion() {
		return listaCalificacion;
	}
	public void setListaCalificacion(List<Calificacion> listaCalificacion) {
		this.listaCalificacion = listaCalificacion;
	}
	public double getPromedio() {
		return promedio;
	}
	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

}
