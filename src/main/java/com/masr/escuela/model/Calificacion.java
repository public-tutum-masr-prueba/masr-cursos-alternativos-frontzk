package com.masr.escuela.model;

import java.util.Date;

public class Calificacion {
	
	private Long idTCalificaciones;
	private Materia materia;
	private Alumnos alumnos;
	private Double calificacion;
	private Date fechaRegistro;
	
	public Long getIdTCalificaciones() {
		return idTCalificaciones;
	}
	public void setIdTCalificaciones(Long idTCalificaciones) {
		this.idTCalificaciones = idTCalificaciones;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materias) {
		this.materia = materias;
	}
	public Alumnos getAlumnos() {
		return alumnos;
	}
	public void setAlumnos(Alumnos alumnos) {
		this.alumnos = alumnos;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	@Override
	public String toString() {
		return "Calificacion [idTCalificaciones=" + idTCalificaciones + ", materia=" + materia + ", alumnos=" + alumnos
				+ ", calificacion=" + calificacion + ", fechaRegistro=" + fechaRegistro + "]";
	}
	
}
