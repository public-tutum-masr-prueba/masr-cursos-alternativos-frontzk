package com.masr.escuela.model.service;

import java.util.Arrays;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.masr.escuela.model.Alumnos;
import com.masr.escuela.model.Calificacion;
import com.masr.escuela.model.CalificacionWrapper;
import com.masr.escuela.model.Materia;
import com.masr.escuela.model.Response200;

@Service("service")
public class CalificacionesServiceImpl {

	private RestTemplate restTemplate;
	
	public CalificacionesServiceImpl() {
		restTemplate = new RestTemplate();
	}
	
	public List<Alumnos> getListAlumnos() {
		String urlApiRest = "http://localhost:8085/cursos-alternativos/alumnos/";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		List<Alumnos> response = Arrays.asList(restTemplate.getForObject(urlApiRest, Alumnos[].class));
		return response;
	}
	
	public List<Materia> getListMaterias() {
		String urlApiRest = "http://localhost:8085/cursos-alternativos/materias/";
		List<Materia> response = Arrays.asList(restTemplate.getForObject(urlApiRest, Materia[].class));
		//ResponseEntity<Object> mig = restTemplate.exchange(urlApiRest, HttpMethod.GET, new HttpEntity<>(headers), Object.class);
		return response;
	}
	
	public Response200 agregarCalificacion(Calificacion objCalificacion) {
		String urlApiRest = "http://localhost:8085/cursos-alternativos/calificaciones/guardar";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("version", "v1");
		HttpEntity<Calificacion> body = new HttpEntity<Calificacion>(objCalificacion, headers);
		HttpEntity<Response200> responseWs = restTemplate.exchange(
				urlApiRest,
		        HttpMethod.POST,
		        body,
		        Response200.class
		);
		return responseWs.getBody();
	}
	
	public Response200 actualizaCalificacion(Calificacion objCalificacion) {
		String urlApiRest = "http://localhost:8085/cursos-alternativos/calificaciones/actualizar/alumno/{idAlumno}";
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("idAlumno", String.valueOf(objCalificacion.getAlumnos().getIdUsuario()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("version", "v1");
		
		HttpEntity<Calificacion> body = new HttpEntity<Calificacion>(objCalificacion, headers);
		HttpEntity<Response200> responseWs = restTemplate.exchange(
				urlApiRest,
		        HttpMethod.PUT,
		        body,
		        Response200.class,
		        pathVariables
		);
		return responseWs.getBody();
	}
	
	public Response200 eliminarCalificacion(Calificacion objCalificacion) {
		String urlApiRest = "http://localhost:8085/cursos-alternativos/calificaciones/eliminar/{idCalificacion}";
		Map<String, Long> pathVariables = new HashMap<String, Long>();
		pathVariables.put("idCalificacion", objCalificacion.getIdTCalificaciones());
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.set("version", "v1");
		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Response200> responseWs = restTemplate.exchange(
				urlApiRest,
		        HttpMethod.DELETE,
		        new HttpEntity<>(headers),
		        Response200.class,
		        pathVariables
		);
		return responseWs.getBody();
	}
	
	public CalificacionWrapper getCalificacionByAlumno(String id) {
		CalificacionWrapper response = null;
		try {
			String urlApiRest = "http://localhost:8085/cursos-alternativos/calificaciones/alumno/{idAlumno}";
			Map<String, String> pathVariables = new HashMap<String, String>();
			pathVariables.put("idAlumno", id);
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Accept", "application/json");
			headers.set("version", "v1");
			HttpEntity<?> entity = new HttpEntity<>(headers);
			
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<CalificacionWrapper> responseWs = restTemplate.exchange(
					urlApiRest,
			        HttpMethod.GET,
			        entity,
			        CalificacionWrapper.class,
			        pathVariables
			);
			//Object response = restTemplate.getForObject(urlApiRest, Object.class, pathVariables);
			response = responseWs.getBody();
			System.out.println(responseWs);
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

}
