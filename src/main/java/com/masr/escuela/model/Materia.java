package com.masr.escuela.model;

public class Materia {
	
	private Long idTMaterias;
	private String nombre;
	private Boolean activo;
	
	public Long getIdTMaterias() {
		return idTMaterias;
	}
	public void setIdTMaterias(Long idTMaterias) {
		this.idTMaterias = idTMaterias;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
}
