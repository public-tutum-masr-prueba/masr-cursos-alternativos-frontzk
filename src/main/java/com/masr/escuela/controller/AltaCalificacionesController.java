package com.masr.escuela.controller;

import java.text.DecimalFormat;
import java.util.List;

//import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Doublespinner;
import org.zkoss.zul.Window;

import com.masr.escuela.model.Alumnos;
import com.masr.escuela.model.Calificacion;
import com.masr.escuela.model.CalificacionWrapper;
import com.masr.escuela.model.Materia;
import com.masr.escuela.model.Response200;
import com.masr.escuela.renders.CalificacionComboBoxRender;
import com.masr.escuela.renders.CalificacionListBoxRender;
import com.masr.escuela.model.service.CalificacionesServiceImpl;

public class AltaCalificacionesController extends ControladorBase {
	
	private static final long serialVersionUID = 1204062473060163434L;
	
	private CalificacionesServiceImpl calificacionesService;
	
	protected Window ventana;
	protected Button btnGuadarCalificacion, btnActualizarCalificacion, btnEliminar;
	protected Doublespinner spinnerCalificacion;
	protected Listbox listboxCalificaciones;
	protected Combobox comboBoxAlumno;
	protected Combobox comboBoxMateria;
	protected Label lblPromedio;
	protected Grid gridBtnDelete;
	private List<Alumnos> listAlumnosWs;
	private List<Materia> listMateriasWs;
	
	public AltaCalificacionesController() {
		//Aqui en el constructor de la clase aun no se renderiza la vista
		this.calificacionesService = new CalificacionesServiceImpl();
		this.listAlumnosWs = calificacionesService.getListAlumnos();
		this.listMateriasWs = calificacionesService.getListMaterias();
	}
	
	public void onCreate$ventana() {
		gridBtnDelete.setVisible(false);
		obtenerDataComboBoxAlumno();
		obtenerDataComboBoxMateria();
	}
	
	// ComboBoxAlumno
	public void obtenerDataComboBoxAlumno() {
		ListModelList<Alumnos> elementos = new ListModelList<Alumnos>(listAlumnosWs);
		comboBoxAlumno.setModel(elementos);
		comboBoxAlumno.setItemRenderer(new CalificacionComboBoxRender());
	}
	
	public void onSelect$comboBoxAlumno() {
		Alumnos objAlumnoCombo = (Alumnos) comboBoxAlumno.getSelectedItem().getAttribute("dataAlumno");
		System.out.println(objAlumnoCombo);
		obtenerListboxCalificaciones(String.valueOf(objAlumnoCombo.getIdUsuario()));
	}
	
	/*public void onChanging$comboBoxAlumno(InputEvent e) {
		Alumnos objAlum = (Alumnos) e.getData();
		Alumnos objAlumnoCombo = (Alumnos) comboBoxAlumno.getSelectedItem().getAttribute("dataAlumno");
		String idAlumno = e.getValue();
		obtenerListboxCalificaciones(idAlumno);
	}*/
	
	// ComboBoxMateria
	public void obtenerDataComboBoxMateria() {
		ListModelList<Materia> elementos = new ListModelList<Materia>(listMateriasWs);
		comboBoxMateria.setModel(elementos);
		comboBoxMateria.setItemRenderer(new CalificacionComboBoxRender());
	}
	
	// LISTBOX
	public void obtenerListboxCalificaciones(String idAlumno) {
		DecimalFormat df = new DecimalFormat("0.00");
		CalificacionWrapper calificacionesWS = calificacionesService.getCalificacionByAlumno(idAlumno);
		ListModelList<Calificacion> modeloListBox = new ListModelList<Calificacion>(calificacionesWS.getListaCalificacion());
		listboxCalificaciones.setModel(modeloListBox);
		listboxCalificaciones.setItemRenderer(new CalificacionListBoxRender());
		lblPromedio.setValue("Promedio General: " + df.format(calificacionesWS.getPromedio()));
		lblPromedio.setStyle("background:#d9d5d5;color:#1233d7;font-weight:bold");
	}
	
	public void onSelect$listboxCalificaciones() {
		Listitem listItemCalificaciones = listboxCalificaciones.getSelectedItem();
		Calificacion objCalificacion = (Calificacion) listItemCalificaciones.getAttribute("selected");
		System.out.print(objCalificacion.toString());
		gridBtnDelete.setVisible(true);
	}
	
	// ELIMINAR
	public void onClick$btnEliminar() {
		Listitem listItemCalificaciones = listboxCalificaciones.getSelectedItem();
		Calificacion objCalificacion = (Calificacion) listItemCalificaciones.getAttribute("selected");
		System.out.print(objCalificacion.toString());
		Response200 responseWs = calificacionesService.eliminarCalificacion(objCalificacion);
		if(responseWs.getSuccess().equalsIgnoreCase("error")) {
			Messagebox.show(responseWs.getMsg(), responseWs.getSuccess(), Messagebox.NO, Messagebox.ERROR);	
		}else {
			Messagebox.show(responseWs.getMsg(), responseWs.getSuccess(), Messagebox.OK, Messagebox.INFORMATION);
			//Actualizamos ListBox Consultar Ws
			obtenerListboxCalificaciones(String.valueOf(objCalificacion.getAlumnos().getIdUsuario()));
			gridBtnDelete.setVisible(false);
		}
	}
	
	// GUARDAR CALIFICACION
	public void onClick$btnGuadarCalificacion() {
		guardaOactualizaCalificacion(1);
	}
	
	// ACTUALIZA CALIFICACION
	public void onClick$btnActualizarCalificacion() {
		guardaOactualizaCalificacion(2);
	}
	
	public void guardaOactualizaCalificacion(int bandera) {
		//Consumimos el Api Rest Para guardar la calificacion
		Response200 responseGuardarWs = null;
		Alumnos objAlumnoCombo = (Alumnos) comboBoxAlumno.getSelectedItem().getAttribute("dataAlumno");
		Materia objMateriaCombo = (Materia) comboBoxMateria.getSelectedItem().getAttribute("dataMateria");
		Calificacion calificacionRequest = new Calificacion();
		calificacionRequest.setAlumnos(objAlumnoCombo);
		calificacionRequest.setMateria(objMateriaCombo);
		calificacionRequest.setCalificacion(spinnerCalificacion.getValue());
		if(1==bandera)
			responseGuardarWs = calificacionesService.agregarCalificacion(calificacionRequest);
		else if(2==bandera)
			responseGuardarWs = calificacionesService.actualizaCalificacion(calificacionRequest);
		
		if(responseGuardarWs.getSuccess().equalsIgnoreCase("error")) {
			Messagebox.show(responseGuardarWs.getMsg(), responseGuardarWs.getSuccess(), Messagebox.NO, Messagebox.ERROR);	
		}else {
			Messagebox.show(responseGuardarWs.getMsg(), responseGuardarWs.getSuccess(), Messagebox.OK, Messagebox.INFORMATION);
			//Actualizamos ListBox Consultar Ws
			obtenerListboxCalificaciones(String.valueOf(objAlumnoCombo.getIdUsuario()));
		}
	}
	
}
