package com.masr.escuela.controller;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;

@SuppressWarnings("rawtypes")
// no sera instanciada solo se hereda
public abstract class ControladorBase extends GenericForwardComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4383439115861512826L;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

}
